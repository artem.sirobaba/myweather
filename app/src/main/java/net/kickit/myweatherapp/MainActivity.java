package net.kickit.myweatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import net.kickit.myweatherapp.model.WeatherMain;
import net.kickit.myweatherapp.modelforcast.MainForeCast;
import net.kickit.myweatherapp.network.StartNetworkingCutTempr;
import net.kickit.myweatherapp.network.StartNetworkingForeCast;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void getForeCast(View view) {
        System.out.println("Button getForecast CLICKED!!!");
        final EditText cityText = findViewById(R.id.inputCity);
        String text = cityText.getText().toString();
        cityText.getText().clear();
        FutureTask<String> future = new FutureTask<>(new StartNetworkingForeCast( text));
        new Thread(future).start();
        String data = null;
        try {
            data = future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(future.isDone()) {
            Intent intent = new Intent(MainActivity.this, ListViewActivity2.class);
            intent.putExtra("DATA", data);
            startActivity(intent);
        }
    }

    public void getCurTempr(View view){
        System.out.println("Button getCurTempr CLICKED!!!");

        final EditText cityText = findViewById(R.id.inputCity);
        String text = cityText.getText().toString();
        cityText.getText().clear();

         new Thread(new StartNetworkingCutTempr(this, text)).start();
    }


}