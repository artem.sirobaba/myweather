package net.kickit.myweatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.kickit.myweatherapp.modelforcast.ForeCast;
import net.kickit.myweatherapp.modelforcast.MainForeCast;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListViewActivity2 extends AppCompatActivity {

    Gson gson = new Gson();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view2);

        String data = getIntent().getStringExtra("DATA");


        MainForeCast mainForeCast = convertToObject(data);

        List<String> displayData = new ArrayList<>();
        for(ForeCast f: mainForeCast.getList()){
            displayData.add(f.toString());
        }

        final ListView list = findViewById(R.id.list);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, displayData);
        list.setAdapter(arrayAdapter);
    }

    public MainForeCast convertToObject(String jsonString){

        Type classType = new TypeToken<MainForeCast>() {}.getType();

        System.out.println(jsonString);
        MainForeCast mainForeCast =  gson.fromJson(jsonString, classType);

        return mainForeCast;
    }
}