package net.kickit.myweatherapp.network;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.kickit.myweatherapp.model.WeatherMain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;


public class StartNetworkingCutTempr implements Runnable {

    private Activity activity;
    private Gson gson;
    private String cityName;

    public StartNetworkingCutTempr(Activity activity, String cityName) {
        this.cityName = cityName;
        this.activity = activity;
        gson = new Gson();
    }

    public String getWeather() throws IOException {
        String url = "http://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&appid=cab7c7daac0813f2a71c7fdfd446f93d";
        String text;

        URL urlUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urlUrl.openConnection();
        conn.connect();
        InputStream in = conn.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        while ((text = reader.readLine()) != null) {
            stringBuilder.append(text);
        }

        return stringBuilder.toString();
    }

    @Override
    public void run() {

        String result = null;
        try {
            result = getWeather();
        } catch (IOException e) {
            e.printStackTrace();
        }

        WeatherMain mainObj =  convertToObject(result);

        final double feelsLikeTemp = convertKelvinToFarinhite(mainObj.getMainTempr().getFeelsLike());

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String temperature = String.valueOf(feelsLikeTemp);
                Toast toast = Toast.makeText(activity, "Temperature in " + cityName + " feels like "
                        + temperature.substring(0, 5), Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    public WeatherMain convertToObject(String jsonString){

        Type classType = new TypeToken<WeatherMain>() {}.getType();

        JsonObject convertedObject = new Gson().fromJson(jsonString, JsonObject.class);
        System.out.println(convertedObject.get("feels_like"));

        System.out.println(jsonString);
        WeatherMain weatherMain =  gson.fromJson(jsonString, classType);

        return weatherMain;
    }

    private double convertKelvinToFarinhite(double kelvin){
        return (((kelvin - 273.0d) * 9.0d/5.0d) + 32.0d);
    }
}
