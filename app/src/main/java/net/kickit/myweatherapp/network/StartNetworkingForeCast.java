package net.kickit.myweatherapp.network;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;


public class StartNetworkingForeCast implements Callable<String> {

    private String cityName;

    public StartNetworkingForeCast(String cityName) {
        this.cityName = cityName;
    }

    public String getWeather() throws IOException {
        String url = "http://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "&appid=cab7c7daac0813f2a71c7fdfd446f93d";
        String text;

        URL urlUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urlUrl.openConnection();
        conn.connect();
        InputStream in = conn.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        while ((text = reader.readLine()) != null) {
            stringBuilder.append(text);
        }

        return stringBuilder.toString();
    }

    @Override
    public String call() throws IOException {
        String result = getWeather();
        return result;
    }
}
